Editing the website content
---------------------------

1. Load `index.html` in your local browser.  Edit.  Reload.

2. Commit to master.

3. Push master to origin (gitlab).

   The Gitlab Continuous Integration mode will notice our
   .gitlab-ci.yml file and build a new Docker image with the rebuilt
   site in it and cut traffic over from the older image to the newer
   one automatically.

   (The status can be viewed in Gitlab: vaark-site > CI/CD >
   Pipelines).

4. To revert an error, just fix master and push again.

What gets pushed to the public website
--------------------------------------

1. Everything under public, which becomes the root of the website.

Releasing a new version of vaark via the website
------------------------------------------------

1. Have vaark-site checked out in .../vaark-site/

2. Optionally: update any documentation on the vaark-site homepage (see above).

3. Have vaark checked out in .../vaark/

4. `cd .../vaark`

5. edit version.txt (increase the number)

6. merge to master, test, etc.

7. push master

8. `./bin/dev-release-publish`

9. The above script will:

   1. Build the tarball

   2. Copy it to ../vaark-site/public/dist/

   3. In the vaark-site project: checkout master, pull, add, commit, and push

   4. Gitlab CI pushes the site to production, per above.
